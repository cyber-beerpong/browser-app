<?php

require_once __DIR__.'/../vendor/autoload.php';

use Ttobsen\Beerpong\Render\Render;
use Steampixel\Route;

const ROUTES = ["/", "/schedule", "/info"];

Route::add("/",         function() {Render::routeDefault();});
Route::add("/schedule", function() {Render::routeSchedule();});
Route::add("/schedule", function() {Render::routeSetResult();}, 'post');
Route::add("/info",     function() {Render::routeInfo();});
Route::add("/register", function() {Render::routeRegister();});
Route::add("/register", function() {Render::routeNewRegister();}, 'post');

Route::run('/');

?>