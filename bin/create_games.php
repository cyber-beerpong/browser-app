<?php

require_once __DIR__.'/../vendor/autoload.php';

use Ttobsen\Beerpong\CLI;
use Ttobsen\Beerpong\Database\Handler;
use Ttobsen\Beerpong\Game\Creator;

const DB_FILE = 'web/database.db';

$db = new Handler(DB_FILE);

CLI\showHeadline("Drop Games");
$db->clearTable("games");

$groups = $db->getGroups();
$group_users = array();

$num_games = 0;
foreach($groups as $group_id => $group) {
    $group_users = $db->getUsers($group_id);
    $games = Creator::build(array_column($group_users, 'id'));

    CLI\showHeadline("Create Group $group_id");
    foreach($games as $game) {
        $db->createGame($group_id, $game['team1'], $game['team2']);
        $db->createGame($group_id, $game['team2'], $game['team1']);
        $name1 = $group_users[$game['team1']]['name'];
        $name2 = $group_users[$game['team2']]['name'];
        echo "Create $name1 vs $name2\n";
        $num_games += 2;
    }
}

CLI\showHeadline("$num_games games created");
