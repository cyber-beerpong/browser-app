<?php

namespace Ttobsen\Beerpong\Database;

const DB_FILE = '../web/database.db';

class Handler
{

    private $db;

    function __construct($db_file = DB_FILE)
    {
        $this->db = new \SQLite3($db_file);
    }

    public function getUsers($group_id = NULL)
    {
        $sql_where = ($group_id == NULL) ? "" : "WHERE group_id = $group_id";
        $sql = "SELECT * FROM users $sql_where";

        $res = $this->db->query($sql);
        $users = array();
        while ($row = $res->fetchArray()) {
            $users[$row['id']] = $row;
        }
        return $users;
    }

    public function getGroups()
    {
        $res = $this->db->query("SELECT * FROM groups");
        $groups = array();
        while ($row = $res->fetchArray()) {
            $groups[$row['id']] = $row;
        }
        return $groups;
    }

    public function clearTable($db_name)
    {
        $this->db->query("DELETE FROM $db_name");
        $this->db->query("VACUUM");
    }

    public function createGame($group_id, $team1, $team2)
    {
        $sql = "INSERT INTO games(group_id, team1, team2) VALUES($group_id, $team1, $team2)";
        $this->db->query($sql);
    }

    public function getGames()
    {
        $res = $this->db->query(
            "SELECT
                user1.name as teamname1, user1.id as team1_id, user2.name as teamname2, user2.id as team2_id,
                groups.name as groupname, games.group_id, games.id,
                games.team1, games.team2, games.pts1, games.pts2
            FROM games
            JOIN users user1 ON games.team1=user1.id
			JOIN users user2 ON games.team2=user2.id
			JOIN groups ON games.group_id=groups.id");
        $games = array();

        while ($row = $res->fetchArray()) {
            $games[$row['group_id']][] = array(
                'id'       => $row['id'],
                'group'    => $row['groupname'],
                'team1'    => $row['teamname1'],
                'team1_id' => $row['team1_id'],
                'team2'    => $row['teamname2'],
                'team2_id' => $row['team2_id'],
                'pts1'     => $row['pts1'],
                'pts2'     => $row['pts2'],
            );
        }
        return $games;
    }

    public function setScore($id, $score1, $score2)
    {

        if ($score1 == 6 && $score2 == 6) return false;
        if ($score1 != 6 && $score2 != 6) return false;

        $sql = "UPDATE games SET pts1=$score1, pts2=$score2 WHERE id=$id";
        $this->db->query($sql);
        return true;
    }

    public function createUser($group_id, $name, $realname)
    {
        $sql = "INSERT INTO users(group_id, name, realname) VALUES($group_id, '$name', '$realname')";
        $this->db->query($sql);
    }

}