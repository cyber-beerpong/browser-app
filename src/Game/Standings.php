<?php

namespace Ttobsen\Beerpong\Game;

use Ttobsen\Beerpong\Database\Handler;

class Standings
{
    public static function create(Handler $db)
    {
        $users = $db->getUsers();
        $games = $db->getGames();

        // Prepare Data
        $standings_data = array();
        foreach ($users as $user) {
            $user_data = $user;
            $user_data['games'] = 0;
            $user_data['legs']  = 0;
            $user_data['pts1']  = 0;
            $user_data['pts2']  = 0;
            $user_data['rank']  = 1;

            foreach ($games[$user_data['group_id']] as $game) {

                $tmp_game = array();

                if ($game['team1_id'] == $user_data['id']) {
                    $tmp_game['pts1'] = $game['pts1'];
                    $tmp_game['pts2'] = $game['pts2'];
                } elseif ($game['team2_id'] == $user_data['id']) {
                    $tmp_game['pts1'] = $game['pts2'];
                    $tmp_game['pts2'] = $game['pts1'];
                } else {
                    continue;
                }

                if (!is_numeric($tmp_game['pts1']) || !is_numeric($tmp_game['pts2'])) continue;

                $user_data['games']++;
                if ($tmp_game['pts1'] > $tmp_game['pts2']) $user_data['legs']++;
                $user_data['pts1'] += $tmp_game['pts1'];
                $user_data['pts2'] += $tmp_game['pts2'];
            }

            $standings_data[$user['group_id']][] = $user_data;
        }

        // Sort Data
        foreach ($standings_data as $key => $standing_data) {
            $legs  = array_column($standing_data, 'legs');
            $pts1  = array_column($standing_data, 'pts1');
            $pts2  = array_column($standing_data, 'pts2');
            $games = array_column($standing_data, 'games');
            array_multisort(
                $legs,  SORT_DESC,
                $games, SORT_DESC,
                $pts1,  SORT_DESC,
                $pts2,  SORT_ASC,
                $standing_data);
            $standings_data[$key] = $standing_data;
        }

        // Create Rank
        foreach ($standings_data as $group_id => $standing_data) {
            for ($i=1; $i < count($standing_data); $i++) {
                $prev_rank = $standings_data[$group_id][$i-1]['rank'];
                $standings_data[$group_id][$i]['rank'] = (
                    self::tieBreaker(
                    $standings_data[$group_id][$i],
                    $standings_data[$group_id][$i-1])) ?
                    $prev_rank + 1:
                    $prev_rank;
            }
        }

        return $standings_data;
    }

    public static function tieBreaker($user1, $user2)
    {
        $params = array('legs', 'games', 'pts1', 'pts2');
        foreach ($params as $param)
            if ($user1[$param] != $user2[$param]) return true;
        return false;
    }


}
