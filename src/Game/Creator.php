<?php

namespace Ttobsen\Beerpong\Game;


class Creator {

    public static function build ($teams = [1, 2, 3, 4], $start_date = 20070101, $interval = 7) {

        // nur gerade Anzahl Teams erlaubt
        $num_teams = count($teams);
        if (($num_teams % 2) != 0)
            trigger_error("Game Creator:only odd number of teams per group allowed!", E_USER_ERROR);


        // --- Spielpaarungen bestimmen ---------------------------------------
        $n      = $num_teams - 1;
        $spiele = array();

        for ($i = 1; $i <= $num_teams - 1; $i++) {
            $h = $num_teams;
            $a = $i;
            // heimspiel? auswärtsspiel?
            if (($i % 2) != 0) {
                $temp = $a;
                $a    = $h;
                $h    = $temp;
            }

            $spiele[] = array('h'        => $h,
                'a'        => $a,
                'spieltag' => $i);

            for ($k = 1; $k <= (($num_teams / 2) - 1); $k++) {

                if (($i-$k) < 0) {
                    $a = $n + ($i-$k);
                }
                else {
                    $a = ($i-$k) % $n;
                    $a = ($a == 0) ? $n : $a; // 0 -> n-1
                }

                $h = ($i+$k) % $n;
                $h = ($h == 0) ? $n : $h;    // 0 -> n-1

                // heimspiel? auswärtsspiel?
                if (($k % 2) == 0) {
                    $temp = $a;
                    $a = $h;
                    $h = $temp;
                }

                $spiele[] = array('h' => $h, 'a' => $a, 'spieltag' => $i);
            }
        }


        // --- Spielplan erstellen --------------------------------------------------
        $spielplan  = array();
        $spiele_cnt = count($spiele);

        for ($x = 0; $x < $spiele_cnt; $x++) {

            $spielplan[$spiele[$x]['spieltag']][] = array(
                'h' => $spiele[$x]['h'],
                'a' => $spiele[$x]['a']);
        }

        $start_date = strtotime($start_date);
        $game_date  = date("Ymd", mktime(0, 0, 0,
            date("m", $start_date) ,
            date("d", $start_date)+$interval,
            date("Y", $start_date)));

        for ($x = 1; $x <= count($spielplan); $x++) {
            $spielplan[$x]['datum'] = $game_date;
            $game_date              = strtotime($game_date);
            $game_date              = date("Ymd", mktime(0, 0, 0,
                date("m", $game_date) ,
                date("d", $game_date)+$interval,
                date("Y", $game_date)));
        }

        $spieltage_cnt = count($spielplan);

        $spiele = array();

        for ($x = 1; $x <= $spieltage_cnt; $x++)
        {

            $spiele_cnt = count($spielplan[$x]) - 1;
            $spieltag   = $spielplan[$x];
            for ($y = 0; $y < $spiele_cnt; $y++) {
                $spiele[] = array('team1' => $spieltag[$y]['h']-1, 'team2' => $spieltag[$y]['a']-1);
            }
        }

        // Randomizer
        $team_ids = range(0, $num_teams-1);
        shuffle($team_ids);

        $games = array();
        foreach ($spiele as $spiel) {
            $games[] = array('team1' => $teams[$team_ids[$spiel['team1']]], 'team2' => $teams[$team_ids[$spiel['team2']]]);
        }

        return $games;
    }
}
