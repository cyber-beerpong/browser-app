<?php

namespace Ttobsen\Beerpong\CLI;

define("HEADLINE_MAX_WIDTH", 80);
define("HEADLINE_SYMBOL", "*");

function showHeadline($text)
{
    echo str_repeat(HEADLINE_SYMBOL, HEADLINE_MAX_WIDTH)."\n";
    echo HEADLINE_SYMBOL.str_pad($text, HEADLINE_MAX_WIDTH-2, " ",  STR_PAD_BOTH).HEADLINE_SYMBOL."\n";
    echo str_repeat(HEADLINE_SYMBOL, HEADLINE_MAX_WIDTH)."\n";
}