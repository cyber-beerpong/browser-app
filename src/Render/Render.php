<?php

namespace Ttobsen\Beerpong\Render;

use Ttobsen\Beerpong\Database\Handler;
use Ttobsen\Beerpong\Game\Standings;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;


const BASE_DIR = "../";
const TEMPLATE_DIR = BASE_DIR . "template/";
const CACHE_DIR    = BASE_DIR . "cache/";
const COMPILE_DIR  = BASE_DIR . "compile/";
const STATIC_DIR   = TEMPLATE_DIR . "static/";
const PLUGIN_DIR   = "../plugins/";

const DEFAULT_CONFIG = array(
    "refresh_route" => "/"
);


class Render
{

    public static function routeDefault()
    {
        $db             = new Handler();
        $standings_data = Standings::create($db);

        $config = DEFAULT_CONFIG;
        $config["users"] = $standings_data[1];

        $twig = self::createTwig();
        echo $twig->render('index.html.twig', $config);

    }

    public static function routeInfo()
    {
        $config = DEFAULT_CONFIG;
        $config['refresh_route'] = "/info";

        $twig = self::createTwig();
        echo $twig->render('info.html.twig', $config);
    }

    public static function routeSchedule()
    {
        $db     = new Handler();
        $games = $db->getGames();

        $config = DEFAULT_CONFIG;
        $config['refresh_route'] = "/schedule";
        $config['games'] = $games[1];

        $twig = self::createTwig();
        echo $twig->render('schedule.html.twig', $config);
    }

    public static function routeSetResult()
    {
        if (is_numeric($_POST['id']) && is_numeric($_POST['score1']) && is_numeric($_POST['score2'])) {
            if ($_POST['score1'] == 6 || $_POST['score2'] == 6) {
                $db = new Handler();
                $db->setScore($_POST['id'], $_POST['score1'], $_POST['score2']);
                unset($db);
            }
        }

        self::routeSchedule();
    }

    public static function routeRegister()
    {
        $db        = new Handler();
        $user_data = $db->getUsers(1);

        $config = DEFAULT_CONFIG;
        $config["users"] = $user_data;

        $twig = self::createTwig();
        echo $twig->render('register.html.twig', $config);
    }

    public static function routeNewRegister()
    {
        if (isset($_POST['name']) && isset($_POST['realname'])) {
            if (!empty($_POST['realname'])) {
                $db = new Handler();
                $db->createUser(1, $_POST['name'], $_POST['realname']);
                unset($db);
            }
        }

        self::routeRegister();
    }

    private static function createTwig()
    {
        $loader = new FilesystemLoader('../template');
        $twig = new Environment($loader);
        $twig->enableDebug();
        return $twig;
    }

}
